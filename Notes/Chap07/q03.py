def sum_list(num_list):
    total = 0               # Start with additive identity
    for number in num_list:             # Loop through each number
        total = total + number         # Accumlate the total
    return total

my_nums = range(0,5+1)                 # Create a list of numbers plus one it because range doesn't include last number
print(sum_list(my_nums))          # Print the call result to test
