product = 1      # Start with the multiplicative identity
numbers = [1, 2, 3, 4, 5]
for number in numbers:
    product = product * number
print(product)              # Prin
