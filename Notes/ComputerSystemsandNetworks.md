# Big Idea 4: Computing Systems and Networks 


## Main ideas

- Built-in **redundancy** enables the Internet to continue working when parts
  of it are not operational.
- Communication on the Internet is defined by **protocol** such as
  TCP/IP that determine how messages to send and receive data are formatted.
- Data sent through the Internet is broken into **packets** of equal size.
- The Internet is **scalable**, which means that new capacity can be quickly
  added to it as demand grows.
- The **World Wide Web** (WWW) is a system that uses the Internet to share
  web pages and data of all types.
- Computing systems can be in **sequential**, **parallel**, and **distributed**
  configurations as they process data.


## Vocabulary

- Bandwidth
> A measurement that indicating the maximum capacity of a wired or wireless comunications link to transmit over a network connection in a given time. This can be measured in bits/kilobits/megabits/gigabits etc.
- Computing device
> Something that can perform substantial computations without human interaction.
- Computing network
> Interconnected computing devices that can exchange data and share resources with each other.
- Computing system
> Integrated devices that input, output, process, and store data and information.
- Data stream
> The transmission of a sequence of digitally encoded coherent signals to convey information
- Distributed computing system
> Computing systems that are split over a network.
- Fault-tolerant
> A system's ability to continue operating without interruption when one or more of its components fail. 
- Hypertext Transfer Protocol (HTTP) 
> An application protocol for distributed, collaborative, hypermedia information systems that allows users to communicate data on the World Wide Web. 
- Hypertext Transfer Protocol Secure (HTTPS) 
> A protocol that secures communication and data transfer between a user's web browser and a website.
- Internet Protocol (IP) address
> A series of numbers that identifies any device on a network
- Packets
> 
- Parallel computing system
> 
- Protocols
> 
- Redundancy
> 
- Router
> 
