# Big Idea 1: Creative Development

## Main ideas

- Collaboration on projects with diverse team members representing a variety
  of backgrounds, experiences, and perspectives can develop a better computing
  product.
- A *development process* should be used to design, code, and implement
  software that includes using feedback, testing, and reflection.
- Software documentation should include the programs requirements, constraints,
  and purpose.
- Software should be adequately tested before it is released.
- *Debugging* is the process of finding and correcting errors in software.


## Vocabulary

- code segment
> Part of an object file or corresponding section of the program's virtual address space that has instructions that are executable.
- collaboration
> Something in which many people work together to achieve a common goal.
- comments
> Notes added to a program that provide information explaining code.
- debugging
> The process of finding and fixing bugs in code.
- event-driven programming
> Something in which the program flow is determined by the actions the user take (ex. mouse/keyboard clicks)
- incremental development process
> A method in software developement where the functionality of the program is split into seperate modules.
- iterative development process
> A method of splitting a larger application into smaller chunks.
- logic error
> These happen when there is a problem in the logic or structure of the program. They can cause the program to yeild unexpected results, but not crash.
- overflow error
> These happen when the data type in a program receives a value greater than value that it can hold.
- program
> A specific set of istructions for a computer to perform.
- program behavior
> How the program behaves.
- program input
> Data that is received by the computer.
- program output
> How the computer represents the result of the program.
- prototype
> Simplified versions of the program usually used for testing and demenstrating parts of a program.
- requirements
> A condition needed by the user to achieve a goal.
- runtime error
> An error that happenes while the program is running.
- syntax error
> Misakes in the code that cause an error message to appear when compiling. (ex. misspellings, punctuation, etc.)
- testing
> The process of evaluating a program to find out if it met the requirements or not.
- user interface (UI)
> Somewhere there is user and computer interaction and communication. (ex. displays, mouse, keyboard, etc.)

## Computing Innovation

A *computer artifact* is anything created using a computer, including apps,
games, images, videos, audio files, 3D-printed objects, and websites.
*Computing innovations* are innovations which include a computer program as a
core part of its functionality.  GPS and digital maps are examples of
computing innovations that build upon the early non-computer innnovation of
map making.


## Development Process 

![Software Development Life Cycle](resources/SDLF.svg)


## Errors

Four types of programming errors need to be understood:

1. Syntax errors
2. Runtime errors
3. Logic errors
4. Overflow errors

Which type of error is this?
```
for i in range(10)
    print(i)
```

> Styntax error

Which type of error is this?
```
# Add the numbers from 1 to 10 and print the result
total = 0
for num in range(10)
    total += num 
print(total)
```
> Syntax error

Which type of error is this?
```
nums = [3, 5, 8, 0, 9, 11]
result = 1
for num in nums:
    result = result / num
print(result)
```

> Runtime error
