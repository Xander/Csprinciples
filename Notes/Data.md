# Big Idea 2: Data 


## Main ideas

- Abstractions such as numbers, colors, text, and instructions can be
  represented by binary data ("It's all just bits!").
- Numbers can be converted from one number system to another. Computers use
  the binary number system ("It's all just bits!" ;-).
- **Metadata** helps us find and organize data.
- Computers can process and organize data much faster and more accurately than
  people can.
- [Data mining](https://en.wikipedia.org/wiki/Data_mining), the process of
  using computer programs to sift through data to look for patterns and trends,
  can lead to new insights and knowledge.
- It is important to be aware of bias in the data collection process.
- Communicating information visually helps get the message across.
- **Scalability** is key to processing large datasets effectively and
  efficiently.
- Increasing needs for storage led to the development of data compression
  techniques, both lossless and lossy.


## Vocabulary

- Abstraction
> The process of removing or generalizing physical, spacial, or temporal details.
- Analog data
> Data represented in a physilcal way
- Bias
> Systematic and reapetable errors in a computer system that creates unfair data/outcomes
- Binary number system
> A number system in which there are two possible values for each digit, a 0 or a 1.
- Bit
> The smallest unit of data that a computer process stores. Also known as a binary digit.
- Byte
> A unit of data that is 8 bits long.
- Classifying data
> The process of analyzing sructured or unstructered data and organizing it into catagories.
- Cleaning data
> The process of fixing or removing incorrect, corrupted, incorrectly formatted, duplicate, or incomplete data within a dataset.
- Digital data
> The electronic representation of data in a format that computers can read and understand.
- Filtering data
> The process of choosing a smaller part of a data set for viewing and/or analyzing data.
- Information
> Organized or classified data.
- Lossless data compression
> Data compression method that allow the original data to be perfectly reconstructed with no loss of data.
- Lossy data compression
> Data compression method in which some data in a file is permenatly removed and not restored after decompression.
- Metadata
> Data that provides information about other data. Examples include: file size, dates (created/modified), author.
- Overflow error
> An error that occurs when the program recieves a value outside it's ability to handle.
- Patterns in data
> A pattern of data that repeats in some reconizable way.
- Round-off or rounding error
> An error that occurs when the computer can't represent an exact number.
- Scalability
> The measure of a system's ability to increase or decrease in performance and cost in response to changes in application and system processing demands.
