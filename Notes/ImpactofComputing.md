# Big Idea 5: Impact of Computing 


## Main ideas

- New technologies can have both beneficial and unintended harmful effects,
  including the presence of bias.
- Citizen scientists are able to participate in identification and problem
  solving.
- The digital divide keeps some people from participating in global and local
  issues and events.
- Licensing of people’s work and providing attribution are essential.
- We need to be aware of and protect information about ourselves online.
- Public key encryption and multifactor authentication are used to protect
  sensitive information.
- Malware is software designed to damage your files or capture your sensitive
  data, such as passwords or confidential information.

##Vocabulary
- Asymmetric ciphers
> Cryptographic systems that consist of a pair of related keys. Each pair has a public and a private key that corrispond to one another.
- Authentication
> A process that determines if someone or something is what is says to be.
- Bias
> An opinion in favor of or agiainst something when compared to something else.
- Certificate Authority (CA)
> Something that stores, signs, and issues digital certificates.
- Citizen science
> Scientific reasearch that is cunducted with perticipation from the public.
- Creative Commons licensing
> A copywrite license that allows for free distribution of a copywrited work. It is used when an author whats to allow other people to share, use, and/or build upon their work.
- Crowdsourcing
> The practice of getting input into a task or information by via a large number of people. This can be paid or unpaid. Crowdsourcing is typically done through the internet.
- Cybersecurity
> The state of being protected against unauthorized or criminal use of electronic data, or the act of preventing this.
- Data mining
> The process of extracting and discovering patterns in a large set of data. 
- Decryption
> The process in which encrypted data is reverted back into it's original form.
- Digital divide
> The divide between those who have access to computers and the internet, and those who do not.
- Encryption
> The process of turning information or data into code.
- Intellectual property (IP)
> Something that is the result of creativity to which someone has rights to and may apply a patent, copywrite, tradmark, etc.
- Keylogging
> Aka keystroke logging or keyboard capturing, is the action of recording the keys pressed on a keyboard usually done so that the person using the keyboard doesn't know they are being monitored.
- Malware
> Any software intentionally designed to cause disruption to the computer, server, client, or network, to leak private information, to gain unauthorized access, or software that interferes with the user's computer security/privacy.
- Multifactor authentication
> An authentication method that involves the user presenting at least 2 peiecs of evidence that they are who they say they are in order to grant them access to something.
- Open access
> A set of principles and practices that reasearch outputs use in order to be distributed online free of any access charges and other barriers.
- Open source
> Software that the copyright holder grants users the ability to use, study, change, or distribute the software and its source code to anyone for any purpose.
- Free software
> Software that grants access to users to run the software for any purpose and to use, study, change, or distribute it and any adapted versions.
- FOSS (Free and Open-Sourced Software)
> A term used to refer to groups of software that contain both free and open sourced software that anyone can use, study, and change. The source code is also openly shared so people can improve the software.
- PII (personally identifiable identification)
> Any information related to someone.
- Phishing
> The practice of messages claiming to be from reputable companies in order to try to get individuals to reveal personal information.
- Plagiarism
> The practice of taking someone elses work or ideas and useing them as their own.
- Public key encryption
> A method of encrypting data with 2 seperate keys. The public key is available to anyone. Data encrypted with the public key can only be decrypted with the corrisponding private key.
- Rouge access point
> A wireless access point that has been installed onto a secure network without authorization from a network adminastrator.
- Targeted marketing
> A strategy that businesses use to try to get a certain group of people to buy their products or use their services.
- Virus
> A program that replicates itself by modifying other computer programs and insesrting its own code.
