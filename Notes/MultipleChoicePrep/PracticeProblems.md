## Practice Problems

[source](https://www.test-guide.com/quiz/ap-comp-sci-1.html)

1. Musicians record their songs on a computer. When they listen to a digitally saved copy of their song, they find the sound quality lower than expected. Which of the following could be a possible reason why this difference exists?

- [ ] The file was moved from the original location, causing some information to be lost
- [ ] The recording was done through the lossless compression technique.
- [ ] The song file was saved with higher bits per second
- [x] The song file was saved with lower bits per second

2. A school had a 90% pass rate for students that took the last AP exam. The school wants to use this achievement to advertise its services to families of prospective students.

Which of the following methods would be most effective in delivering this information to the families in a summarized manner?

- [ ] Email the prospective families that have middle-school-age children.
- [ ] Create a report based on the student body's overall performance for a marketing pamphlet.
- [x] Post an interactive pie chart about the topics and scores of the passing students on the schools' website
- [ ] Post the results of the passing students on social media sites.

3. A programmer is designing a code to analyze the number of people in middle age. The standard is set to be 42, and only citizens above this age are considered middle-aged citizens.

The algorithm can be designed with two different conditional statements, shown below. Will the two statements have the same operations?

- [x] No
- [ ] Yes
- [ ] Only if all ages are 42
- [ ] Only if all ages are even numbers above 42
