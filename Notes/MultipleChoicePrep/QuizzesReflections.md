## Quiz 1

### Question 4

An automobile company uses GPS technology to implement a smartphone app for its customers that they can use to call their self-driving electric cars to their location. When the app is used, the coordinates of the car owner are sent to their car. The car then proceeds to drive itself from the parking lot to its owner.
- Idling periods for which the car is running in anticipation of the signal would cause excess fuel consumption
- Unauthorized access to vehicles through the app
- User location data could be used by companies and third-party stakeholders
- Interference of signals if two people use the app simultaneously and close by

I chose b becuase I misinterpreted privacy as security.

The correct answer is c becuase you are sending the app your location which is now viewable by companies that want to steal you data.

### Question 5

A programmer has developed an algorithm that computes the sum of integers taken from a list. However, the programmer wants to modify th ecode to sum up even numbers from the list. What programming structure can the programmer add to do this?
- Sequencing
- Iteration
- Searching
- Selection

I chose sequencing because I thought it was the process of repeating an action over and over until you reach the end. However, I goit this confused with iteration. I also did not see when it said the programmer only wanted to add the even numbers, I thought it was all numbers. 

The correct answer is Selection becuase you need to select the even numbers in order to add them together.

## Quiz 2

### Question 6

An image stored on a computer contains pixels that represent how bright the red, green, and blue values are. The most common format for pixels is to represent the red, green, and blue, useing 8 bits, which vary from 0 to 255. If the current red value is 10011101, what would be the new value in binary if the red value increased by 4?
- 157bin
- 0100bin
- 10011111bin
- 10100001nbin

I chose c because I accidentally started counting from 2 instead of 1 so I added 2 instead of 4 to the value.

The correct answer is d because 4 in bin is 100, if you add that to 10011101, you will end up with 10100001.

### Question 9

Using a binary search, how many iterations would it take to find the letter 'w'?
str <- [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z]
- 2
- 3
- 23
- 24

I chose 23 becuase I didn't know what a binary search was and used a linear search instead for some reason.

The correct answer is 3 becuase a binary search cuts the data in half, and if the item you are looking for is in the half, it cuts that in half and it repeats this process until only the item is left. This means, you only have to split this data in half 3 times before it reaches 'w'.

#### Question 10

Using a linear search, how many iterations will it take to find the letter 'x'?
str <- [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z]
- 3
- 4
- 23
- 24

I chose 23 because I thought it used the same letter as the previous question, however I didn't read the whole question to find out I was wrong.

The correct answer is 24 becuase if you go from left to right, you will reach the letter 'x' after 24 letters.

## Quiz 3

### Question 5

When customers log into their account on an online shirts retail store, they can access information about the selected shirts before making their purchase.

Some information about the shirts include ID, type, price, size, color, and available units of the selected shirt. What additoinal inforamation can be derived from the available dataset?

- # of shirts sold in prev. moth
- What type of shirt is popular among children
- What color shirt men prefer
- What shirt is currently out of stock

I chose c becuase I did not see that it said the available units. Also, I thought the company was the one looking at the data, so I assumed that the company was able to see how many of each shirt people with mens sizes people bought in the same color.

The correct answer is d becuase the customer is able to see if 0 shirts are available which would mean that that shirt is out of stock.

### Question 6

The same task is performed through sequential and parallel computing models. The time taken by the sequential setup was 30 seconds, while it took the parallel model only 10 seconds.

What will be the speedup parameter for working with the parallel model from now on?

- 0.333
- 3
- 300
- 20

I chose 3 becuase I thought it was asking what you would change the speed parameter of the parallel model not the sequential model.

The correct answer is 1/3 or about 0.333 which is option a. This is becuase you are multiplying 30 by 1/3 which would be equal to 10.

## Quiz 4

### Question 3

A team of researchers wants to create a program to analyze the amount of pollution reported in roughly 3,000 counties across the United States. The program is intended to combine county data sets and then process the data. Which of the following is most likely to be a challenge in creating the program?
- A computer program cannot combine data from different files 
- Different conties organize data in different ways
- The number of conties is too large for the program to process
- The total number of rows of data is too large for the program to process

I chose a because I thought b was not going to be a problem and I thought some programs couldn't combine data from different files.

The correct answer is b becuase if differnt conties organized data in different ways, you would have to reorganize all the data in one way to make it easier to process.

### Question 5

A database of information about shows at a concert venue contains the following information.
1. Name of artist performing at the show
2. Date of show
3. Total dollar amount of all tickets sold
Which of the following additional pieces of information would be the most useful in determining the artist with the greatest attendance during a particular month?
- Average ticket price
- Length of show in minutes
- Start time of the show
- Total dollar amount of food and drinks sold during the show

I chose c because I thought the question was asking what piece of information would help determine the artist that performed the most not how many people attended.

The correct answer is a because if you know the total dollar amount and the average ticket price, you can find out about how many people went to each show.

### Question  8

COnsider the following procedure called mystery, it is intended to display the number of times a number target appears in a list

'''
PROCEDURE mystery (list, target)
  count <- 0
  FOR EACH n IN list
    IF (n = target)
      count <- count + 1
    ELSE
      count <- 0
DISPLAY (count)
'''
Which of the following best describes the behavior of the procedure? Select two answers.
- The program correctly displays the count if the target is not in the list
- The program never correcly displays the correct values
- The program correctly displays the correct value if the target appears once in the list and also in the end of the list
- The program always correctly displays the correct value for count

I only chose a becuase I did not see that it said select 2 answers and I though c was saying it appears twice, once in the list and once at the end of the list.

The correct answers were a and c because if the target is not in the list, it will always be 0 no matter what. If the target only appears at the end, the count will always display as 1 no matter what. If the target apears in the middle somewhere, the count would go back to 0 whenever there is a value that is not the target value.
