from gasp import *          # So that you can draw things
from random import randint

class Player:
    pass
class Robot:
    pass

begin_graphics()            # Create a graphics window
game_over = False
numbots = 2

def place_robots():
    global robots
    robots = Robot()
    #numbots = Robot()
    robot = []
    for r in range(numbots):
        robots.x = randint(0, 63)
        robots.y = randint(0, 47)
        global robot_shape
        robot_shape = Box((10 * robots.x, 10 * robots.y), 10, 10, filled=True)
        robot.append(robots)

def place_player():
    global player
    player = Player()
    player.x = randint(0, 63)
    player.y = randint(0, 47)
    global player_shape
    while player.x == robots.x and player.y == robots.y:
        player.x = randint(0, 63)
        player.y = randint(0, 47)
    player.shape=Circle((10 * player.x + 5, 10 * player.y + 5), 5, filled=True)

def move_player():
    global player
    key = update_when('key_pressed')
    remove_from_screen (player.shape)
    player.shape=Circle((10 * player.x + 5, 10 * player.y + 5), 5, filled=True)
    if key == "w" and player.y < 47:
        player.y += 1
    elif key == "a" and player.x > 0:
        player.x -= 1
    elif key == "s" and player.y > 0:
        player.y -= 1
    elif key == "d" and player.x < 63:
        player.x += 1
    elif key == "q" and player.x > 0 and player.y < 47:
        player.y += 1
        player.x -= 1
    elif key == "e" and player.x < 63 and player.y < 47:
        player.y += 1
        player.x += 1
    elif key == "z" and player.x > 0 and player.y > 0:
        player.y -= 1
        player.x -= 1
    elif key == "c" and player.x < 63 and player.y > 0:
        player.y -= 1
        player.x += 1
    elif key == "x":
        remove_from_screen(player.shape)
        place_player()
    move_to(player.shape, (10 * player.x + 5, 10 * player.y + 5))

def move_robots():
    global robots
    global robot_shape
    global player
    remove_from_screen(robot_shape)
    robot_shape = Box((10 * robots.x, 10 * robots.y), 10, 10, filled=True)
    if player.x > robots.x:
        robots.x += 1
    if player.y > robots.y:
        robots.y += 1
    if player.x < robots.x:
        robots.x -= 1
    if player.y < robots.y:
        robots.y -= 1
    move_to(robot_shape, (10 * robots.x, 10 * robots.y))

def check_collisions():
    global game_over
    if player.x == robots.x and player.y == robots.y:
        game_over = True
        Text("Game Over! You've been caught!", (320, 240), size = 40) 
        sleep(5)

place_robots()
place_player()

while not game_over:
    move_player()
    move_robots()
    check_collisions()

end_graphics()              # Finished!
