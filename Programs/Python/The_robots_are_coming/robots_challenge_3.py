from gasp import *
begin_graphics()

ball_x = 5
ball_y = 5
ball = Circle((ball_x, ball_y), 5, filled=True)
while ball_x < 635:
    move_to(ball, (ball_x + 4, ball_y + 3))
    ball_x += 4
    ball_y += 3
    sleep(0.02)
