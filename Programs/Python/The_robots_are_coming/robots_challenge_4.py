from gasp import *

begin_graphics()

key_text = Text("a", (320, 240), size=48)
key_text2 = Text("b", (380, 290), size=48)

while True:
    key1 = update_when('key_pressed')
    key2 = update_when('key_pressed')
    remove_from_screen(key_text)
    remove_from_screen(key_text2)
    key_text = Text(key1, (320, 240), size=48)
    key_text2 = Text(key2, (380, 290), size = 48)
    if key1 == 'q':     # See Sheet C if you don't understand this
        break          # See Sheet L if you aren't sure what this means

end_graphics()
