from turtle import *      # use the turtle library
space = Screen()          # create a turtle screen (space)
bob = Turtle()            # create a turtle named bob

# Make a square
bob.forward(100)          # tell bob to move forward by 100 units
bob.right(90)             # turn right by 90 degrees
bob.forward(90)          # tell bob to move forward by 100 units
bob.right(90)             # turn right by 90 degrees
bob.forward(100)          # tell bob to move forward by 100 units
bob.right(90)             # turn right by 90 degrees
bob.forward(90)          # tell bob to move forward by 100 units

# Position for roof
bob.right(90)

# Make a roof
bob.forward(100)          # tell bob to move forward by 100 units
bob.right(-120)           # turn LEFT by 120 degrees
bob.forward(100)          # tell bob to move forward by 100 units
bob.right(-120)           # turn LEFT by 120 degrees
bob.forward(100)          # tell bob to move forward by 100 units
bob.left(180)
bob.forward(25)
bob.right(-30)
bob.forward(50)
bob.right(90)
bob.forward(15)
bob.right(90)
bob.forward(24)
bob.right(30)
bob.forward(55)
bob.left(30)
bob.forward(90)
bob.left(90)
bob.forward(60)
bob.left(90)
bob.forward(30)
bob.right(90)
bob.forward(15)
bob.right(90)
bob.forward(30)
bob.left(90)
bob.forward(1500)
bob.left(180)
bob.forward(10000000000000)
