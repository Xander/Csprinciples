# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> 
> The purpose of the program is to create a playble game. The player is a circle and the robots are boxes. The player wants to avoid the robots and try to get the robots to run into each other.
>
2. Describes what functionality of the program is demonstrated in the
   video.
>
> Every time the player moves, the robots move towards the player. If 2 robots collide into eachother or a piece of junk, they become junk. If all of the robots become junk, the game will move on to the next level. If all of the robots on the last level become junk, the game ends and the player wins. If a robot collides with the player in any level, the came ends and the player loses.
>
3. Describes the input and output of the program demonstrated in the
   video.
>
> The input is the player pressing keys, and the output is the player moving then the robots moving to the player.
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
>
> ![list](list.png) 
> The list "robots" stores the data of the positions of the robots on the screen. It is declared on line 35, and filled on lines 37 - 43.
>

2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
>
> ![list_use](list_use.png)
> The code goes through all of the robots in the list (line 91), and moves them toward the player (lines 92 - 103)
>

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
>
>  This list's name is robot. (declared line 35)
>
2. Describes what the data contained in the list represent in your
   program
>
> The data in the list represents all of the robots on the board.
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
>
> If you did not have this list in the code, it would be very difficult to change the number of robots.
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
>
>
> ![function](function.png)
> The procedure's name is 'collided' (line 106). The parameters are 'thing1' and 'list_of_things' (line  106) where 'thing' is a robot. The function checks if a robot is in the same location as something else (line 108). The function will loop through all the robots on the board to find out if one of them collided with an object.
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
>
> ![function_use](function_use.png)
> The procedure is used when the code checks to see if the player is in the same spot as a robot.
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
>
> The procedure checks to see if a robot is in the same location as something else. If so, it returns 'True' (line 108).
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
>
> The function loops through all of the items in the list, while doing so, it checks to see if one of the items has the same x and y positions as another item.
>
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
>
> The procedure checks to see if a robot is in the same location as the player, if it is, it will return true. If it is not, it will return false.
>
>
> Second call:
>
> The procedure checks if a robot is in the same location as a piece of junk. If it is, it will return true. If not it will return false.
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
>
> The conditions that are tested in the first call are if a robot is in the exact same spot as the player.
>
> Condition(s) tested by the second call:
>
> The condition that is being tested the second time is if a robot is in the exact same spot as a piece of junk.
>
>
3. Identifies the result of each call
>
> Result of the first call:
>
> The procedure will return true if a robot is in the same location as the player. If not, it will return false (108 - 110).
>
>
> Result of the second call:
>
> The procedure will return true if a robot is in the same location as a dead robot (junk). If not, it will return false (108 - 110).
>
>
