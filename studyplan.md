How is the APCSP exam scored?
- After taking the APCSP exam, you will get a score from 1-5, 5 being the best and 1 being the worst.
- 70% of the score is from the 70 multiple choice questions. 57 of the multiple choice questions are single select multiple choice, 5 are single select multiple choice with a reading passage about computer innovation, and 8 of the questions are multi select multiple choice questions where you have to select 2 answers.
- The other 30% is the create performance task where you develop a computer program of your choice. You get at least 12 hours of in class time to do this.
